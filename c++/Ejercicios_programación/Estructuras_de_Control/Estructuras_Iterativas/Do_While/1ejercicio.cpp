#include <stdio.h>
#include <stdlib.h>

    int main(){

    int op; 

    do{ 
      
      printf("Escoje un número situado entre el 1 y el 10: ");	    
      scanf("%i", &op);

    if (op >=1 && op <=10){ // Si el número se encuentra entre 1 y 10.
    
      printf("Tu número es: %i\n",op);
}  else 
      printf("El número que has intrducido no es válido, vuelve a introducir un número.\n");

} 
       while(op >10 || op <1); //Si el número no se encuentra entre 1 y 10, se repite el bucle.
   
    return 0;
}
