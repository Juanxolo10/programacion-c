#include <stdio.h>
#include <stdlib.h>

int main(){

  int n;
  int c=0; 
  int sum=0; 
  int media;

  do{
    printf("Escribe un numero positivo: ");
    scanf("%i", &n);

  if(n>0){
    sum = sum + n; //Operación de sumar los numeros.
    c++; //Incrementa el contador.
}
} while(n>0);

    media = sum / c; //Calculamos la media.
    printf("La media es: %i\n", media); //Imprimimos la media en caso de que el número sea negativo.

  return 0;
}
