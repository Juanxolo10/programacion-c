#include <stdio.h>
#include <stdlib.h>

int globl = 0xDEADBEEF;

int main() {

    int op1, op2, op3, result;

    op1 = 7;
    op2 = 9;
    op3 = 10;
    result = op1 + op2 + op3;

    printf("%i + %i + %i = %i\n", op1, op2, op3, result);

    return EXIT_SUCCESS;

}

